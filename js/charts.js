/**
 * @file
 * JavaScript code for displaying charts using Google Chart API.
 */

// Load Charts library before "onload" event or jQuery's "ready" event.
// See http://stackoverflow.com/a/7476462
if (typeof(google.visualization) == 'undefined') {
  google.load("visualization", "1", {packages:["corechart"]});
}

(function($) {

  /**
   * Renders charts if they are defined in settings.
   */
  Drupal.behaviors.mailStatisticsCharts = {
    attach: function(context, settings) {

      // Check whether there are charts to render.
      if (typeof(settings.mailStatistics) == 'undefined' || typeof(settings.mailStatistics.charts) == 'undefined') {
        return;
      }

      // Render each chart.
      $.each(settings.mailStatistics.charts, function(chartName, chart) {
        $(chart.selector).once(function() {
          var gChart = new google.visualization[chart.type](this);
          gChart.draw(google.visualization.arrayToDataTable(chart.data), chart.options);
        });
      });
    }
  };

})(jQuery);
