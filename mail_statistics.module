<?php

/**
 * @file
 * The module provides features for gathering statistics about outgoing emails.
 */

/**
 * Implements hook_menu().
 */
function mail_statistics_menu() {
  $items = array();

  // Settings.
  $items['admin/config/system/mail_statistics'] = array(
    'title' => 'Mail Statistics',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mail_statistics_settings_form'),
    'access arguments' => array('administer mail statistics'),
    'file' => 'includes/mail_statistics.admin.inc',
  );

  // Statistics callbacks.
  $items['mail-link/%'] = array(
    'page callback' => 'mail_statistics_click',
    'page arguments' => array(1),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['mail-open/%'] = array(
    'page callback' => 'mail_statistics_open',
    'page arguments' => array(1),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  // Main report pages.
  $items['admin/reports/mail_statistics'] = array(
    'title' => 'Mail Statistics',
    'page callback' => 'mail_statistics_report_page',
    'access arguments' => array('access mail statistics reports'),
    'file' => 'includes/mail_statistics.main_report.inc',
  );
  $items['admin/reports/mail_statistics/%/%'] = array(
    'title callback' => 'mail_statistics_main_report_page_title',
    'title arguments' => array(3, 4),
    'page callback' => 'mail_statistics_report_page',
    'page arguments' => array(3, 4),
    'access arguments' => array('access mail statistics reports'),
    'type' => MENU_CALLBACK,
    'file' => 'includes/mail_statistics.main_report.inc',
  );

  // Report pages for detailed report.
  $items['admin/reports/mail_statistics/%/%/%'] = array(
    'title' => 'Opens/clicks timeline',
    'page callback' => 'mail_statistics_timeline_report_page',
    'page arguments' => array(3, 4, 5),
    'access arguments' => array('access mail statistics reports'),
    'type' => MENU_CALLBACK,
    'file' => 'includes/mail_statistics.detailed_report.inc',
  );
  $items['admin/reports/mail_statistics/%/%/%/timeline'] = array(
    'title' => 'Opens/clicks timeline',
    'access arguments' => array('access mail statistics reports'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );
  $items['admin/reports/mail_statistics/%/%/%/recipients'] = array(
    'title' => 'Recipients',
    'page callback' => 'mail_statistics_recipients_report_page',
    'page arguments' => array(3, 4, 5),
    'access arguments' => array('access mail statistics reports'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/mail_statistics.detailed_report.inc',
    'weight' => 10,
  );
  $items['admin/reports/mail_statistics/%/%/%/popular_links'] = array(
    'title' => 'Popular links',
    'page callback' => 'mail_statistics_popular_links_report_page',
    'page arguments' => array(3, 4, 5),
    'access arguments' => array('access mail statistics reports'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/mail_statistics.detailed_report.inc',
    'weight' => 20,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function mail_statistics_permission() {
  return array(
    'administer mail statistics' => array(
      'title' => t('Administer Mail Statistics'),
    ),
    'access mail statistics reports' => array(
      'title' => t('Access Mail Statistics reports'),
    ),
  );
}

/**
 * Implements hook_module_implements_alter().
 */
function mail_statistics_module_implements_alter(&$implementations, $hook) {
  // The module should be the last one which alters mails.
  if ($hook == 'mail_alter') {
    $group = $implementations['mail_statistics'];
    unset($implementations['mail_statistics']);
    $implementations['mail_statistics'] = $group;
  }
}

/**
 * Implements hook_mail_alter().
 *
 * Alters mails: cancels sending, splits recipients apart, updates mail body for
 * each particular recipient, and sends mail for them separately.
 */
function mail_statistics_mail_alter(&$message) {

  // Do not consider mails which are not pretend to be sent.
  if (!$message['send']) {
    return;
  }

  // For testing purposes sent time could be overridden.
  $timestamp = module_invoke('mail_statistics_test', 'sent_time') ?: REQUEST_TIME;

  // Do not send original mail.
  $message['send'] = FALSE;

  _mail_statistics_include('mail_process.inc');
  $mail_id = _mail_statistics_register_mail($message, $timestamp);

  // For testing purposes, store $send_id in special function.
  module_invoke('mail_statistics_test', 'last_mail_id', $mail_id);

  // If there are several recipients in the "To" field, split them apart, then
  // send them separately.
  $recipients = _mail_statistics_get_mail_recipients($message['to']);
  foreach ($recipients as $recipient) {
    $send_id = _mail_statistics_register_send($mail_id, $recipient['user_id'], $recipient['address'], $timestamp);
    $message['to'] = $recipient['address'];

    // As mail_statistics is the last module which implements hook_mail_alter(),
    // there is no need to use drupal_mail() for sending mails. So, send mails
    // "manually" at this point.
    _mail_statistics_process_and_send_mail($message, $send_id, $recipient['user_id']);
  }
}

/**
 * Callback for "mail-link/%" path.
 *
 * Registers click and redirects user to original link URL.
 *
 * @param string $link_uuid
 *   The link UUID.
 *
 * @return int
 *   If link wasn't registered, returns MENU_NOT_FOUND.
 */
function mail_statistics_click($link_uuid) {
  _mail_statistics_include();

  // Ensure that given link was registered. Also get required info.
  $query = db_select('mail_statistics_link', 'l')
    ->fields('l', array('url', 'id', 'send_id'))
    ->condition('l.link_uuid', $link_uuid);
  $query->innerJoin('mail_statistics_send', 's', 's.id = l.send_id');
  $query->addField('s', 'mail_id');
  $query->innerJoin('mail_statistics_mail', 'm', 'm.id = s.mail_id');
  $query->addField('m', 'send_time');
  $info = $query->execute()->fetchAssoc();
  if ($info) {
    _mail_statistics_register_click($info['id'], REQUEST_TIME, $info['mail_id'], $info['send_id'], $info['send_time']);
    // Redirect user to destination URL.
    drupal_goto($info['url']);
  }
  else {
    return MENU_NOT_FOUND;
  }
}

/**
 * Callback for "mail-open/%" path.
 *
 * Registers that mail was opened and delivers 1x1 pixel transparent PNG image
 * to user's browser.
 *
 * @param string $mail_uuid
 *   The mail UUID.
 */
function mail_statistics_open($mail_uuid) {
  _mail_statistics_include();
  // Check that mail was registered. Also get required info.
  $query = db_select('mail_statistics_send', 's')
    ->fields('s', array('id', 'mail_id'))
    ->condition('s.mail_uuid', $mail_uuid);
  $query->innerJoin('mail_statistics_mail', 'm', 'm.id = s.mail_id');
  $query->addField('m', 'send_time');
  $info = $query->execute()->fetchAssoc();
  if ($info) {
    _mail_statistics_register_open(REQUEST_TIME, $info['mail_id'], $info['id'], $info['send_time']);
  }

  // Serve the image to browser in any case.
  $file = DRUPAL_ROOT . '/' . drupal_get_path('module', 'mail_statistics') . '/files/transparent.png';
  if (ob_get_level()) {
    ob_end_clean();
  }
  drupal_add_http_header('Content-Type', 'image/png');
  drupal_add_http_header('Content-Length', filesize($file));
  drupal_add_http_header('Cache-Control', 'public');
  drupal_add_http_header('Expires', gmdate('D, d M Y H:i:s \G\M\T', REQUEST_TIME));
  drupal_send_headers();
  readfile($file);
  drupal_exit();
}

/**
 * Title callback for the main report page.
 *
 * @param string $module
 *   Machine module name.
 * @param string $key
 *   Mail key.
 *
 * @return string
 *   A title for main report page.
 */
function mail_statistics_main_report_page_title($module, $key) {
  _mail_statistics_include();
  return t('Mail Statistics overview for @module_key', array(
    '@module_key' => _mail_statistics_get_module_name($module) . ':' . $key,
  ));
}

/**
 * Shortcut for module_load_include().
 *
 * @param string $type
 *   The include file's type (file extension).
 */
function _mail_statistics_include($type = 'inc') {
  module_load_include($type, 'mail_statistics', 'includes/mail_statistics');
}
