<?php

/**
 * @file
 * Specific functions for mail processing.
 */

/**
 * Stores the information about original mail.
 *
 * @param array $message
 *   The $message array as it was passed to the hook_mail_alter().
 * @param int $timestamp
 *   A unix timestamp.
 *
 * @return int
 *   A mail_id of registered email.
 */
function _mail_statistics_register_mail($message, $timestamp) {
  _mail_statistics_include();
  $campaign = _mail_statistics_detect_campaign($message);
  $fields = array(
    'module' => $message['module'],
    'mail_key' => $message['key'],
    'campaign_id' => $campaign['id'],
    'campaign_name' => $campaign['name'],
    'to_address' => $message['to'],
    'subject' => $message['subject'],
    'body' => implode("\n\n", $message['body']),
    'send_time' => _mail_statistics_format_time_string($timestamp),
  );
  $mail_id = db_insert('mail_statistics_mail')->fields($fields)->execute();
  return $mail_id;
}

/**
 * Returns parsed email "To" string. Tries to find user by email address.
 *
 * @param string $to_string
 *   RFC 2822 addresses list.
 *
 * @return array
 *   An indexed array of arrays containing two keys: "address" and "user_id".
 *   If the user is not found by mail address, then "user_id" become a zero.
 */
function _mail_statistics_get_mail_recipients($to_string) {
  // At the moment, function simply parses addresses without names.
  // @todo (alex): Depending on exist PHP extensions, use something like
  // mailparse_rfc822_parse_addresses() or similar here to get recipient names.
  preg_match_all('/\b[A-Z0-9._%+-]+@(?:[A-Z0-9-]+\.)+[A-Z]{2,6}\b/i', $to_string, $matches);
  $addresses = $matches[0];

  // If something went wrong, return original $to_string.
  if (empty($addresses)) {
    return array(
      array(
        'address' => $to_string,
        'user_id' => 0,
      ),
    );
  }

  $recipients = array();
  $mail_to_user_id_map = db_select('users', 'u')
    ->condition('u.mail', $addresses, 'IN')
    ->fields('u', array('mail', 'uid'))
    ->execute()
    ->fetchAllKeyed();
  foreach ($addresses as $address) {
    $recipients[] = array(
      'address' => $address,
      'user_id' => isset($mail_to_user_id_map[$address]) ? $mail_to_user_id_map[$address] : 0,
    );
  }
  return $recipients;
}

/**
 * Stores information about an email which was sent for particular recipient.
 *
 * @param int $mail_id
 *   ID of original mail.
 * @param int $user_id
 *   User ID. Or zero if user was not found by mail address.
 * @param string $to_address
 *   An email address with or without recipient name (RFC 2822 address).
 * @param int $timestamp
 *   The timestamp when original mail was sent.
 *
 * @return int
 *   A send_id of registered email.
 */
function _mail_statistics_register_send($mail_id, $user_id, $to_address, $timestamp) {
  _mail_statistics_include();
  $fields = array(
    'mail_uuid' => _mail_statistics_generate_uuid(),
    'mail_id' => $mail_id,
    'user_id' => $user_id,
    'to_address' => $to_address,
  );
  $send_id = db_insert('mail_statistics_send')->fields($fields)->execute();
  _mail_statistics_register_action('send', $send_id, $timestamp, $mail_id, $send_id, _mail_statistics_format_time_string($timestamp));
  return $send_id;
}

/**
 * Returns mail body with links replaced to ours.
 *
 * Replacement type is depended on "mail_statistics_link_parse_type" variable.
 *
 * @param string $body
 *   Mail message body.
 * @param int $send_id
 *   The ID of a sent mail: {mail_statistics_send}.id.
 *
 * @return string
 *   Message body with processed links.
 */
function _mail_statistics_process_links($body, $send_id) {
  switch (variable_get('mail_statistics_link_parse_type', 'html')) {

    // Replace only links in the "href" attributes.
    case 'html':
      // First find all HTML pieces.
      preg_match_all('/<html>.*?<\/html>/is', $body, $matches);
      $html_pieces = $matches[0];
      $html_replacement = array();
      foreach ($html_pieces as $html) {

        // Find all links and register them.
        preg_match_all('/(\shref=)("|\')(http.*?)("|\')(\s|>)/is', $html, $link_matches);
        $links = array_unique($link_matches[3]);
        $links_map = _mail_statistics_register_links($links, $send_id);
        foreach ($links_map as $link => $link_uuid) {

          // Replace links with ours.
          $link_replace_pattern = '/(\shref=)("|\')(' . preg_quote($link, '/') . ')("|\')(\s|>)/is';
          $new_url = _mail_statistics_construct_link_url($link_uuid);
          $link_replacement = '$1$2' . $new_url . '$4$5';
          $html = preg_replace($link_replace_pattern, $link_replacement, $html);
        }
        $html_replacement[] = $html;
      }

      // Finally, replace all HTML pieces with processed.
      $body = str_replace($html_pieces, $html_replacement, $body);
      break;

    // Simply replace all links.
    case 'all':
      $url_pattern = '#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#';
      preg_match_all($url_pattern, $body, $link_matches);
      $links = array_unique($link_matches[0]);
      $links_map = _mail_statistics_register_links($links, $send_id);
      $links_map = array_map('_mail_statistics_construct_link_url', $links_map);
      $body = strtr($body, $links_map);
      break;
  }
  return $body;
}

/**
 * Returns absolute URL basing on given $link_uuid.
 *
 * @param string $link_uuid
 *   The link UUID.
 *
 * @return string
 *   Absolute URL.
 */
function _mail_statistics_construct_link_url($link_uuid) {
  return url('mail-link/' . $link_uuid, array('absolute' => TRUE, 'alias'=>TRUE));
}

/**
 * Registers links and returns link-to-uuid map.
 *
 * @param array $links
 *   An array of URLs to register.
 * @param int $send_id
 *   The ID of a sent mail: {mail_statistics_send}.id.
 *
 * @return array
 *   An array where keys are actual URLs and values are link UUIDs.
 */
function _mail_statistics_register_links($links, $send_id) {
  _mail_statistics_include();
  $links_map = array();
  foreach ($links as $link) {
    $fields = array(
      'link_uuid' => _mail_statistics_generate_uuid(),
      'send_id' => $send_id,
      'url' => $link,
    );
    db_insert('mail_statistics_link')->fields($fields)->execute();
    $links_map[$link] = $fields['link_uuid'];
  }
  return $links_map;
}

/**
 * Returns mail body with hidden image appended to "body" tag.
 *
 * @param string $body
 *   Message body.
 * @param int $send_id
 *   The ID of a sent mail: {mail_statistics_send}.id.
 *
 * @return string
 *   Message body with hidden image appended.
 */
function _mail_statistics_append_hidden_image($body, $send_id) {
  $mail_uuid = db_select('mail_statistics_send', 's')
    ->fields('s', array('mail_uuid'))
    ->condition('s.id', $send_id)
    ->execute()
    ->fetchField();
  $url = url('mail-open/' . $mail_uuid, array('absolute' => TRUE));
  $image = '<img src="' . $url . '" />';
  $body = str_replace('</body>', $image . '</body>', $body);
  return $body;
}

/**
 * Processes mail body and sends it.
 *
 * A mail sends "manually". drupal_mail() function is not used here because
 * a mail is already altered by all modules and we don't want to break mail
 * system sending emails separately.
 *
 * Basically, the function code repeats last part of drupal_mail() function
 * with the only difference: mail body altered after it's formatted. This is
 * necessary to replace links correctly and insert hidden image.
 *
 * @param array $message
 *   The $message variable as it was passed to the hook_mail_alter(), but with
 *   "to" key updated to particular recipient.
 * @param int $send_id
 *   The ID of a sent mail: {mail_statistics_send}.id.
 */
function _mail_statistics_process_and_send_mail($message, $send_id) {
  $system = drupal_mail_system($message['module'], $message['key']);
  $message = $system->format($message);

  // Alter mail body.
  $message['body'] = _mail_statistics_process_links($message['body'], $send_id);
  $message['body'] = _mail_statistics_append_hidden_image($message['body'], $send_id);

  if (!$system->mail($message)) {
    watchdog('mail', 'Error sending e-mail (from %from to %to).', array('%from' => $message['from'], '%to' => $message['to']), WATCHDOG_ERROR);
    drupal_set_message(t('Unable to send e-mail. Contact the site administrator if the problem persists.'), 'error');
  }
}

/**
 * Tries to detect campaign parameters for known modules.
 *
 * @param array $message
 *   The $message variable as it was passed to the hook_mail_alter().
 *
 * @return array
 *   An array with "id" and "name" keys.
 */
function _mail_statistics_detect_campaign($message) {
  $campaign = array(
    'id' => '',
    'name' => '',
  );

  // Detect Simplenews campaign.
  if (module_exists('simplenews') && isset($message['id']) && in_array($message['id'], array('simplenews_node', 'simplenews_test'))) {
    $node = $message['params']['simplenews_source']->getNode();
    $campaign['id'] = $node->nid;
    $campaign['name'] = $node->title;
  }

  return $campaign;
}
