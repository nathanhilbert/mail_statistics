<?php

/**
 * @file
 * Detailed report pages.
 */

/**
 * Page callback for "Opens/clicks timeline" report.
 *
 * @param string $module
 *   Machine module name.
 * @param string $key
 *   Mail key.
 * @param string $time_or_campaign
 *   Time string in "YmdHis" format (or part of it) or campaign ID.
 *
 * @return array
 *   A render array.
 */
function mail_statistics_timeline_report_page($module, $key, $time_or_campaign) {
  _mail_statistics_include();
  _mail_statistics_fix_title_and_breadcrumbs($module, $key, $time_or_campaign);
  $build = array();
  $build['form'] = drupal_get_form('mail_statistics_detailed_report_form', TRUE);
  $header = array(
    'time' => array(
      'data' => t('Date/Time'),
      'field' => 'time_cut',
      'sort' => 'asc',
    ),
    'opens_count' => array(
      'data' => t('Mail opens'),
      'field' => 'opens_count',
      'sort' => 'desc',
    ),
    'clicks_count' => array(
      'data' => t('Link clicks'),
      'field' => 'clicks_count',
      'sort' => 'desc',
    ),
  );

  $query = db_select('mail_statistics_action', 'a');
  $query->innerJoin('mail_statistics_mail', 'm', 'm.id = a.mail_id AND m.module = :module AND m.mail_key = :key', array(':module' => $module, ':key' => $key));
  if (_mail_statistics_get_setting('group_by') == 'time') {
    $query->where(_mail_statistics_get_time_expression('m.send_time') . ' = :send_time AND a.action IN (:actions)', array(
      ':send_time' => $time_or_campaign,
      ':actions' => array('open', 'click'),
    ));
  }
  else {
    $query->condition('m.campaign_id', $time_or_campaign);
    $query->condition('a.action', array('open', 'click'), 'IN');
  }
  $query->addExpression(_mail_statistics_get_time_expression('a.time', FALSE), 'time_cut');
  $query->addExpression("SUM(a.action = 'open')", 'opens_count');
  $query->addExpression("SUM(a.action = 'click')", 'clicks_count');
  $query->groupBy('time_cut');

  $query = $query
    ->extend('TableSort')
    ->orderByHeader($header);
  $query = $query
    ->extend('PagerDefault')
    ->limit(_mail_statistics_get_setting('rows_per_page_detailed'));

  $table_rows = array();
  $chart_data = array();

  foreach ($query->execute() as $row) {
    $table_row = array(
      'time' => _mail_statistics_format_time($row->time_cut),
      'opens_count' => check_plain($row->opens_count),
      'clicks_count' => check_plain($row->clicks_count),
    );
    $table_rows[] = $table_row;
    $chart_data[$row->time_cut] = $table_row;
  }

  if (variable_get('mail_statistics_display_charts', TRUE) && count($chart_data) > 1) {
    // For the chart use date sorting always.
    ksort($chart_data);
    array_unshift($chart_data, array(
      (_mail_statistics_get_setting('group_by') == 'time') ? t('Date/Time') : t('Campaign'),
      t('Mail opens'),
      t('Link clicks'),
    ));
    $build['chart'] = array(
      '#type' => 'container',
      '#attributes' => array('id' => 'mail-statistics-chart'),
    );
    _mail_statistics_add_chart(array(
      'selector' => '#' . $build['chart']['#attributes']['id'],
      'type' => 'LineChart',
      'data' => $chart_data,
    ));
  }

  $build['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $table_rows,
    '#empty' => t('No results found.'),
  );
  $build['pager'] = array(
    '#theme' => 'pager',
  );

  return $build;
}

/**
 * Page callback for "Recipients" report.
 *
 * @param string $module
 *   Machine module name.
 * @param string $key
 *   Mail key.
 * @param string $time_or_campaign
 *   Time string in "YmdHis" format (or part of it) or campaign ID.
 *
 * @return array
 *   A render array.
 */
function mail_statistics_recipients_report_page($module, $key, $time_or_campaign) {
  _mail_statistics_fix_title_and_breadcrumbs($module, $key, $time_or_campaign);
  $build = array();
  $build['form'] = drupal_get_form('mail_statistics_detailed_report_form');
  $header = array(
    'to_address' => array(
      'data' => t('Mail address'),
      'field' => 'to_address',
    ),
    'user' => array(
      'data' => t('User'),
    ),
    'opens_count' => array(
      'data' => t('Mail opens'),
      'field' => 'opens_count',
    ),
    'clicks_count' => array(
      'data' => t('Link clicks'),
      'field' => 'clicks_count',
      'sort' => 'desc',
    ),
  );

  $query = db_select('mail_statistics_action', 'a');
  $query->innerJoin('mail_statistics_mail', 'm', 'm.id = a.mail_id AND m.module = :module AND m.mail_key = :key', array(':module' => $module, ':key' => $key));
  $query->innerJoin('mail_statistics_send', 's', 's.id = a.send_id');
  $query->addField('s', 'user_id');
  $query->addField('s', 'to_address');
  $query->addExpression("SUM(a.action = 'open')", 'opens_count');
  $query->addExpression("SUM(a.action = 'click')", 'clicks_count');
  if (_mail_statistics_get_setting('group_by') == 'time') {
    $query->where(_mail_statistics_get_time_expression('m.send_time') . ' = :send_time', array(':send_time' => $time_or_campaign));
  }
  else {
    $query->condition('m.campaign_id', $time_or_campaign);
  }
  $query->groupBy('s.to_address');

  $query = $query
    ->extend('TableSort')
    ->orderByHeader($header);
  $query = $query
    ->extend('PagerDefault')
    ->limit(_mail_statistics_get_setting('rows_per_page_detailed'));

  $table_rows = array();
  foreach ($query->execute() as $row) {
    $user = user_load($row->user_id);
    $table_rows[] = array(
      'to_address' => check_plain($row->to_address),
      'user' => ($user && $user->uid) ? theme('username', array('account' => $user)) : '',
      'opens_count' => check_plain($row->opens_count),
      'clicks_count' => check_plain($row->clicks_count),
    );
  }

  $build['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $table_rows,
    '#empty' => t('No results found.'),
  );
  $build['pager'] = array(
    '#theme' => 'pager',
  );

  return $build;
}

/**
 * Page callback for "Popular links" report.
 *
 * @param string $module
 *   Machine module name.
 * @param string $key
 *   Mail key.
 * @param string $time_or_campaign
 *   Time string in "YmdHis" format (or part of it) or campaign ID.
 *
 * @return array
 *   A render array.
 */
function mail_statistics_popular_links_report_page($module, $key, $time_or_campaign) {
  _mail_statistics_fix_title_and_breadcrumbs($module, $key, $time_or_campaign);
  $build = array();
  $build['form'] = drupal_get_form('mail_statistics_detailed_report_form');
  $header = array(
    'url' => array(
      'data' => t('Link'),
      'field' => 'url',
    ),
    'unique_clicks_count' => array(
      'data' => t('Unique clicks'),
      'field' => 'unique_clicks_count',
      'sort' => 'desc',
    ),
    'clicks_count' => array(
      'data' => t('Total clicks'),
      'field' => 'clicks_count',
      'sort' => 'desc',
    ),
  );

  // First select clicks grouped by link_id.
  $subquery = db_select('mail_statistics_click', 'c');
  $subquery->innerJoin('mail_statistics_action', 'a', 'a.action_id = c.id AND a.action = :action', array(
    ':action' => 'click',
  ));
  $subquery->innerJoin('mail_statistics_mail', 'm', 'm.id = a.mail_id AND m.module = :module AND m.mail_key = :key', array(
    ':module' => $module,
    ':key' => $key,
  ));
  $subquery->innerJoin('mail_statistics_link', 'l', 'l.id = c.link_id');
  $subquery->addField('c', 'link_id');
  $subquery->addField('l', 'url');
  $subquery->addExpression('COUNT(*)', 'clicks_count');
  $where = (_mail_statistics_get_setting('group_by') == 'time')
    ? _mail_statistics_get_time_expression('m.send_time') . ' = :time_or_campaign'
    : 'm.campaign_id = :time_or_campaign';
  $subquery->where($where, array(':time_or_campaign' => $time_or_campaign));
  $subquery->groupBy('l.id');

  // In the main query group subquery results by URLs.
  $query = db_select($subquery, 'result');
  $query->addField('result', 'url');
  $query->addExpression('SUM(result.clicks_count)', 'clicks_count');
  $query->addExpression('COUNT(*)', 'unique_clicks_count');
  $query->groupBy('result.url');

  $query = $query
    ->extend('TableSort')
    ->orderByHeader($header);
  $query = $query
    ->extend('PagerDefault')
    ->limit(_mail_statistics_get_setting('rows_per_page_detailed'));

  $table_rows = array();
  foreach ($query->execute() as $row) {
    $table_rows[] = array(
      'url' => l($row->url, $row->url),
      'unique_clicks_count' => check_plain($row->unique_clicks_count),
      'clicks_count' => check_plain($row->clicks_count),
    );
  }

  $build['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $table_rows,
    '#empty' => t('No results found.'),
  );
  $build['pager'] = array(
    '#theme' => 'pager',
  );

  return $build;
}

/**
 * Form callback: detailed report settings.
 */
function mail_statistics_detailed_report_form($form, &$form_state) {
  $time_grouping = !empty($form_state['build_info']['args'][0]);

  drupal_add_css(drupal_get_path('module', 'mail_statistics') . '/css/common.css');
  $form['#attributes']['class'][] = 'mail-statistics-report-form';

  if ($time_grouping) {
    $form['time_grouping_detailed'] = array(
      '#type' => 'select',
      '#title' => t('Group time by'),
      '#default_value' => _mail_statistics_get_setting('time_grouping_detailed'),
      '#options' => _mail_statistics_get_time_grouping_options(),
    );
  }

  $form['rows_per_page_detailed'] = array(
    '#type' => 'select',
    '#title' => t('Rows per page'),
    '#default_value' => _mail_statistics_get_setting('rows_per_page_detailed'),
    '#options' => array(10 => 10, 20 => 20, 50 => 50, 100 => 100),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

/**
 * Submit callback for detailed report settings form.
 */
function mail_statistics_detailed_report_form_submit($form, &$form_state) {
  _mail_statistics_set_setting('rows_per_page_detailed', $form_state['values']['rows_per_page_detailed']);
  if (isset($form_state['values']['time_grouping_detailed'])) {
    _mail_statistics_set_setting('time_grouping_detailed', $form_state['values']['time_grouping_detailed']);
  }
}

/**
 * Updates page title and breadcrumbs for better usability.
 *
 * @param string $module
 *   Machine module name.
 * @param string $key
 *   Mail key.
 * @param string $time_or_campaign
 *   Time string in "YmdHis" format (or part of it) or campaign ID.
 */
function _mail_statistics_fix_title_and_breadcrumbs($module, $key, $time_or_campaign) {
  _mail_statistics_include();
  $module_key = _mail_statistics_get_module_name($module) . ':' . $key;
  $title = t('@module_key - @time_or_campaign', array(
    '@module_key' => $module_key,
    '@time_or_campaign' => (_mail_statistics_get_setting('group_by') == 'time')
      ? _mail_statistics_format_time($time_or_campaign)
      : _mail_statistics_get_campaign_name($module, $time_or_campaign),
  ));
  drupal_set_title($title);
  drupal_set_breadcrumb(array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('Reports'), 'admin/reports'),
    l(t('Mail Statistics'), 'admin/reports/mail_statistics'),
    l($module_key, 'admin/reports/mail_statistics/' . $module . '/' . $key),
  ));
}

/**
 * Returns campaign name by its ID.
 *
 * @param string $module
 *   Machine module name.
 * @param string $campaign_id
 *   Campaign ID.
 *
 * @return string
 *   Campaign title.
 */
function _mail_statistics_get_campaign_name($module, $campaign_id) {
  $campaign_name = db_select('mail_statistics_mail', 'm')
    ->condition('m.module', $module)
    ->condition('m.campaign_id', $campaign_id)
    ->fields('m', array('campaign_name'))
    ->execute()
    ->fetchField();
  return $campaign_name ? $campaign_name : t('(no name)');
}
