<?php

/**
 * @file
 * Admin pages for Mail Statistics module.
 */

/**
 * Module settings form.
 */
function mail_statistics_settings_form() {
  $form['mail_statistics_link_parse_type'] = array(
    '#type' => 'radios',
    '#title' => t('Link parse type'),
    '#default_value' => variable_get('mail_statistics_link_parse_type', 'html'),
    '#options' => array(
      'html' => t('Replace only HTML links'),
      'all' => t('Replace all links'),
    ),
    '#description' => t('<p>The recommended parse type is "Replace only HTML links". In this case will be replaced only URLs placed in the "href" attributes of "a" tags. For instance, <pre>&lt;a href="http://example.com/"&gt;http://example.com/&lt;/a&gt;</pre> will become <pre>&lt;a href="http://your-site.com/mail-link/{uuid}"&gt;http://example.com/&lt;/a&gt;</pre> So, the text of a link will not be replaced and user will not see our URL.</p><p>If the "Replace all links" type is chosen, all URLs will be replaced. Even if plain-text format is used.</p>'),
  );
  $form['mail_statistics_display_charts'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display charts'),
    '#default_value' => variable_get('mail_statistics_display_charts', TRUE),
    '#description' => t('Allow charts displaying (using Google charts API) on report pages.'),
  );
  return system_settings_form($form);
}
